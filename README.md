# BuS Zusammenfassung 

## Einleitung & Grundlagen

### Interrupts

|               |                |     |         |     |              |
| --- | --- | --- | --- | --- | --- |
| Anwendung     | `fread`        |     |         |     |              |
|               | ↓1 ↑11         |     |         |     |              |
| Library       | `read`         |     |         |     |              |
|               | ↓2 ↑10         |     |         |     |              |
| I/O Subsystem | `I/O read`     |     |         |     |              |
|               | ↓3 ↑9          |     |         |     |              |
| Gerätetreiber | `Driver Read`  | ← 8 | Tasklet | ← 7 | ISR          |
|               | ↓ 4            |     |         |     |              |
|               | Schlafen legen |     |         |     | ↑ 6          |
|               |                |     |         |     | `System ISR` |
|               |                |     |         |     | ↑ 5 ISR      |



## Prozesse & Threads

### Prozesse
* Besitzt alles selbst, dauert länger zum starten,
Kommunikation durch normale Prozessk. möglich
* `fork()`

### Threads
* Besitzt selbst:
  * Stack
  * Program Counter
  * Registerwerte
  * Status (running, blocked, ...)
* Teilt sich mit anderen Threads im Prozess:
  * Adressraum
  * globale Variablen, geöffnete Dateien
  * CPU-Zeit
  * Kindprozesse, Signale
* `clone()`

## Scheduling

### Scheduling-Strategien
* Präemptiv: Prozesse können untebrochen werden
* Nicht-* : *
* FIFO/FCFS: Ready-Queue, alle werden nach Reihenfolge abgearbeitet
* LIFO: FIFO mit Stack
* LIFO-PR: Neue Prozesse werden sofort gestartet und nachher
  die anderen abgearbeitet
* SPT/SJF (Shortest Processing Time / Shortest Job First)
* SRPT (Shortest Remaining Processing Time): Kommt ein Job an,
  dessen Bedienzeit kürzer ist als der momentane, bediene zuerst diesen
* SERPT: Shortest Expected Remaining Processing Time*
* HRN (Highest Response Ratio Next):
  * Lange Prozesse können Punkte sammeln,
  die ihre Bedienpriorität steigern
  * Response Ratio r = (Wartez. + Bedienz) / Bedienz.
  * Prozess mit größtem Wert r wird als nächster bedient

### CPU-Scheduling

* Round Robin
  * FIFO Warteschlange
  * Quantum Q, jeder Prozess bekommt CPU zur Dauer von Q
  * Q -> $\infty$: FIFO, Q -> 0: Processor-Sharing
  * Kontextwechsel kann auch Dauer haben
* Highest Priority First:
  * Prioritätsklassen
  * Prozess aus Klasse k wird bedient, falls:
    * CPU frei oder Klasse >k bedient
    * Alle Klassen 1 - (k-1) momentan unbesetzt
  * z.B. rechenintensive Prozesse niedrige Prio und 
    IO-lastige Prozesse hohe Prio
  * Priority Aging
    * Scheduler erniedrigt regelm. Prio der Prozesse
* RR + HPF: Multilevel-Feedback-Queuing
  * Neu ankommende Prozesse bekommen Prio-Klasse
  * Abarbeitung nach Round-Robin
  * Hohe Prio -> Kleinere Quanten
  * Wird Prozess nicht fertig -> nächst niedrigere Prio-Klasse
  * Bedient wird ältester Job mit höchster Klasse
* Rate Monotonic
  * Rechenzeit jedes Prozesses ist konstant & bekannt
  * Deadline = Dauer einer Periode
  * Kürzeste Periode = Höchste Prio
  * Jedes mal, wenn ein neuer Prozess hinzukommt, wird dieser
    evt. vorgelassen
* Earliest Deadline First
  * Wie RM, aber:
  * Früheste Deadline = Höchste Priorität
* Least Laxity First
  * Führe jeden Prozess so spät wie möglich aus

### Mehrkernsysteme
* Dualcore teilt L3-Cache, eigener L1 und L2 Cache
* Cache-Trashing
  * Prozess(-Teil) wird in Caches geladen
  * Prozess wechselt zwischen CPUs (CPU-Hopping)

## Synchronisation

### Semaphoren und Atomare Operationen
* Erzeuger E, Verbraucher V
* Problem: Race Conditions
* Ringpuffer
  * E legt Produkt im ersten freien Platz ab
  * V entnimmt Produkt aus ältestem belegten Platz
  * Puffer hat `MAX - 1` belegte Komponenten
  * `in` und `out` zwei getrennte Variablen zur Mod. des Puffers
* Wechselseitiger Ausschluss
  * Sperrfunktion, Freigabe Funktion, Atomare Operation
  * Korrekte Lösung: Mutual Exclusion, Progress, Bounded Waiting
  * Peterson-Algorithmus: Kombination von Token (Zahl des Prozesses)
    und Flag (Booleans)
  * Für zwei Proz: 
  ```Go
    for true {
      flag[i] = true
      turn = j
      for (flag[j] && turn = j) {}
      critical_section()
      flag[i] = false
    }
  ```
  * Bakery-Algorithmus 
    * n Kunden befinden sich in Warteschlange
    * Nummern-Ziehen (kritischer Bereich)
    ```Go
      number := make([]int, i)
      for true {
        choosing[i] = true
        number[i] = 1 + max(number[0:n-1])
        choosing[i] = false
        for _, j := range number {
          for choosing[j] {}
          for number[j] != 0 && (number[j] < number[i] || (number[j] == number[i] && j < i)) {}
        }
        critical_section()
        number[i] = 0
      }
    ```
  * Einfacher: Atomare Operationen
    * Ausschalten von Interrupts
    * Problem: Benutzerprozess darf nicht einfach Interrupts ausschalten
      → Vordefinierte atomare Operationen
  * Semaphoren
    * Integer-Variable mit atomaren Operationen (verletzt Bounded Waiting)
    ```Go
      func wait() {
        for s == 0 {}
        s -= 1
      }
      
      func signal() {
        s += 1
      }
    ```
    * Mutex: Semaphor mit Anfangswert 1
    * Zählsemaphore: Mit Anfangswert > 1
    * Alternative Implementierung: Mit Queue
* Synchronisationsszenarien
  * Erzeuger/Verbraucher
    * n Erzeuger, m Verbraucher
    * Exklusiver Zugriff auf Lager
    * Drei Semaphoren:
      1. Mutex für Zutritt zu Lager
      2. Semaphor mit Anfangswert 0: Anzahl Elemente im Lager
      3. Semaphor mit Anfangswert MAX: MAX-n Elemente im Lager
  * Reader / Writer
    * Writer hat exkl. Zugriff, Reader dürfen simultan lesen
    * Reader müssen nur warten falls ein Writer im kritischen Bereich ist
    * Sobald Writer wartet, wird neuen Readern der Zugriff verwehrt
    * Fairness durch Lese- / Schreibphasen 
    * Semaphor für Schreibphase, für Lesephase
  * 5-Philosophen-Problem
    * Lösung: Ein Philosoph wird zum Rechtshänder erklärt
    * Mutex für jedes Stäbchen
  * 3-Raucher-Problem
* Semaphoren unter Linux
  * `sem_init` (unbenannt), `sem_open` (benannt), `sem_destroy`
  * `sem_wait` (blockiert), `sem_trywait` (blockiert nicht), `sem_post` (freigeben)
  * `semget` (anlegen einer neuen Semaphore)
  * `semctl` (Kontrolloperation)

### Erweiterte Konstrukte

* Bedingte Kritische Region
  * `region v when b do s`
  * v ist eine Variable innerhalb s
  * Wenn v wahr ist, betrete s und manipuliere v
  * Wenn v falsch ist oder jemand in s ist, warte
  * Implementation mit 2 Warteschlangen und einem Mutex:
    1. Neuer Prozess kommt an
    2. Wenn noch kein Prozess in der Warteschlange ist, locke den Mutex.
    3. Wenn die Bedingung wahr ist, führe den kritischen Bereich aus und unlocke
        den Mutex. Sonst reihe dich in queue 1 ein.
    4. Neuer Prozess kommt an
    5. Ein Prozess ist in queue x, also unlocke nach dem fertig werden nicht den Mutex,
        sondern queue x. In dieser queue rufen dann alle Prozesse sich hintereinander auf
        und prüfen ihre Bedinung, danach führen sie sich aus oder reihen sich in die andere
        Queue ein.
    6. Der letzte Prozess unlockt den Mutex.
* Monitor
  * Menge von Prozeduren und Daten
  * zum Beispiel in Java
* Resource Allocation Graph
  * Knoten des Graphen: Ressourcen, Betriebsmittel
  * Kanten des Graphen: 
    * Anforderung eines Betriebsmittels durch einen Prozess (P -> B)
    * Belegung eines Betriebsmittels durch einen Prozess (B -> P)
  * Wait-For-Graph: Resource Allocation Graph ohne Betriebsmittel
  * Deadlock tritt auf wenn:
    1. Circular Wait: geschlossene Kette im Graphen
    2. Exclusive Use: Keine gemeinsame Nutzung von Betriebsmitteln möglich
    3. Hold & Wait: Weitere Anforderungen sind erlaubt ohne Rückgabe
    4. Non-Preemption: Entzug von Betriebsmitteln ist nicht möglich
  * Deadlocks verbieten
    * Prevention: Deadlocks durch verbieten einer der Bedingungen unmöglich machen
    * Avoidance: dynamisches Verhindern von Deadlocks  
  * Deadlocks erlauben
    * Detection: Entdecken und beseitingen von Deadlocks
    * Suspection: Entdecken von deadlockartigen Situationen
    * Ignoring: Ignorieren    
  * Betriebsmittel-Hierarchie
  * Prozessfortschrittsdiagramm
    * Für 2 Prozesse: Schreibe in Tabelle wann Prozess A oder B gewisse Betriebsmittel benötigt
      und finde dementsprechend einen Fortschritt, mit dem das System Deadlockfrei den 
      Prozess ausführen kann
  * Banker-Algorithmus
    * Gegeben:
      1. Maximal zusätzlich benötigte Betriebsmittel jedes Prozesses, Typs
      2. Aktuell angeforderte Betriebsmittel jedes Prozesses, Typs
      3. Aktuell gehaltene Betriebsmittel jedes Prozesses, Typs
      4. Zur Zeit verfügbare Betriebsmittel jedes Typs
    * Zunächst seien alle Prozesse unmarkiert
    * Führe durch:
      1. Schaue ob es einen unmarkierten Prozess gibt, der weniger Betriebsmittel erfordert als
          frei sind. Wenn nein, gehe zu Schritt 3.
      2. Nehme an, der Prozess läuft jetzt und gibt die Betriebsmittel frei. Füge diese also 
          hinzu und markiere den Prozess.
      3. Wenn jetzt alle Prozesse markiert sind, dann ist das System deadlockfrei.
    * $\mathcal{O}(m * n^2)$

## Speicherverwaltung

### Hauptspeicherverwaltung

* Probleme
  * Arbeitsspeicher kleiner als Adressraum eines Prozesses
  * Benötigter Arbeitsspeicher aller laufenden Prozesse?
* Virtueller Speicher
  * Virtuell mehr Speicher als vorhanden
  * Lade nur Programmteile und Daten, die gerade benötigt werden
  * → Memory Management Unit (MMU) der CPU
* Segment:
  * Logische Einheit zusammenhängender Information, z.B.
    * Unterprogramm (Funktion)
    * Programmdaten
    * Variablen
    * Stack
  * Besitzt einen Namen und (variable) Länge
  * Logische Adresse: `Segment-Nr, Offset`
  * Segmenttabelle
  * Vorteile
    * Trennung von Code & Daten, Zugriffsrechte, Speicherschutz, Gemeinsame Verwendung
      von Segmenten, variable Länge für verschiedene Inhalte
  * Nachteile
    * Externe Fragmentierung des Arbeitsspeichers
    * Begrenzung des Adressraums durch Arbeitsspeicher
  * Strategien zur Wahl eines freien Speicherbereichs
    * First Fit (FF)
      * Platziere das Segment in die nächste passende Lücke
    * Rotating First Fit (RFF)
      * Wie FF, aber ab zuletzt verwendeter Lücke
    * Best Fit (BF)
      * Platziere das Segment in die kleinste passende Lücke
    * Worst Fit (WF)
      * Platziere das Segment in die größte passende Lücke
      * uff
    * Quick Fit (QF)
      * Verwalte getrennte Listen für häufigste Lückengrößen und verwende gezielt eine Liste
  * Defragmentierung
  * Verwaltung freier und belegter Speicherbereiche im Kernel 
    * Bitmap 
      * 1 belegte Einheit, 0 freie Einheiten
    * Verkettete Liste
      * Speicherbereiche durch Startadresse und Länge charakterisieren
      * Flag zur Markierung von frei / belegt
    * Buddy-System
      * Mögliche Speichergrößen vorschreiben (2er-Potenzen)
      * Baumstruktur
      * Segmentverwaltung mit Buddy-System
        * Führe Liste $L_{max}, L_{max-1}, \cdots, L_{min}$
        * $L_{pot}$ Liste freier Blöcke der Größe $2^{pot}$
        * Zu Beginn ist $L_{max} \neq \emptyset$ und die restlichen Listen leer
      * Einfügen eines Segments
        * $L_{pot} \neq \emptyset$: Platziere Segment in einer Lücke und lösche
          den verwendeten Block aus der Liste
        * $L_{pot} = \emptyset$: Suche freien Block in
          $L_{pot + 1}, \cdots, L_{pot + n}$ und spalte diesen in zwei Hälften
          (Buddies) bis eine passende Größe entsteht
      * Freigabe von Segmenten
        * Prüfe ob zugehöriger Buddy frei ist, wenn ja: verschmelze beide
        * Wiederhole bis keine Buddies mehr vereinigt werden können
* Paging
  * Verwalte Speicher als Rahmen fester Länge (2er-Potenz), auf x86 4KiB
  * Verwalte logischen Adressbereich eines Programms in Seiten (Pages) einheitlicher
    Größe
  * Verwalte eine Liste aller freien Rahmen
  * Programme haben Größe "n Rahmen"
  * Logische Adresse: (page number | offset)
  * Page Number nachschaubar in Page Table
  * Translation Look Aside Buffer (TLB)
    * Innerhalb der MMU, 64-1024 Einträge
    * Ein Teil der Page Table wird im TLB gehalten, um den Zugriff durch
      die CPU zu beschleunigen
  * → Page Tables werden so groß, dass sie selbst in Pages unterteilt werden müssen
    → Hierarchisches Paging
    * Zwei- bis vierstufige Page Tables
    * Logische Adresse: (p1|p2|p3|p4|offset)
      * p1 liefert Index für Global Directory,
        durch die Upper Directory identifiziert wird
      * p2 liefert Index für Middle Directory
      * p3 liefert Index für Page Table
      * p4 liefert Index für Rahmen
    * Variante: Verwende Hashing zum Reduzieren der Tabelleneinträge
    * Auch möglich: Segmentierung mit Paging
  * Verwaltungsbits
    * (Rahmennummer|p|d|a|w)
      * p: page_present
      * d: page_dirty
      * a: page_accessed
      * w: page_rw
      * ...
  * MMU prüft ob Seite im Speicher ist 
    * Hit: Seite ist im Speicher
    * Page Fault: Seite muss geladen werden
      1. Page Fault Handler
      2. Prozess ggfs. schlafen legen
      3. Fehlende Seite lokalisieren und nachladen
      4. Rahmenadresse in Page Table verzeichnen
      5. Prozess weiter fortführen
  * Mittlere effektive Zugriffszeit: 
    $t_{eff} = (1-p) \cdot t_{Speicherzugriff} + p \cdot t_{Seitenfehler}$
  * Welche Seite soll verdrängt werden?
    * Demand-Paging: Seitenaustausch nur im Falle eines Seitenfehlers,
      Nachladen der benötigten Seite
    * Demand-Prepaging: Wie Demand-Paging, jedoch ist bei Seitenfehler
      der Austausch mehrerer Seiten möglich
    * Look-Ahead: Es können Seiten ausgetauscht werden, ohne dass ein Seitenfehler
      vorliegt
  * Demand-Paging
    * FIFO
      * Seiten werden in der Reihenfolge ihres Alters verwiesen
    * Least Recently Used (LRU)
      * Seiten die genutzt werden, werden verjüngt
    * LRU Second Chance
      * Die älteste Seite mit nicht gesetztem A-Bit wird ersetzt
      * Ältere Seiten mit gesetzem A-Bit werden verjüngt und das A-Bit gelöscht
      * Clock-Algorithmus
        * Ring-Buffer zur Verwaltung
        * Vorderer Zeiger setzt A-Bit auf 0, hinterer Zeiger verweist auf älteste
          Seite
        * Bei Seitenfehler: Ersetze angezeigte Seite, falls A-Bit = 0
        * Sonst, rücke zur nächsten Position mit A-Bit = 0 vor und ersetze
        * Gehe eins weiter
    * NRU - Not Recently used
      * Zusätzlich wird das D-Bit verwendet
      * Klassen:
        1. A = 0, D = 0
        2. A = 0, D = 1
        3. A = 1, D = 0
        4. A = 1, D = 1
      * Verdränge immer Seite aus der nächsten nichtleeren Klasse
    * CLIMB
      * Aufstieg bei Bewährung
      * Ist die Seite bei einem Zugriff bereits im Speicher vorhanden,
        steigt sie eine Position ein
      * Ist die Seite nicht vorhanden, wird sie als älteste Seite eingefügt
    * Erweitere Seiteneinträge um Zugriffszähler
    * Ersetze bei Seitenfehler Seite mit niedrigstem Zählwert
    * Least Frequently Used
      * Bei jedem Zugriff wird inkrementiert
    * Not Frequently Used
      * In Zeitintervallen werden die Seiten, die benutzt wurden, inkrementiert
    * Most Frequently Used
      * Ersetze Seite mit zweitniedrigstem Wert
    * OPT
      * Optimal: Die Seite, die am längsten nicht mehr gebraucht werden wird
      * Implementierung unmöglich
    * Working Set Model
      * In einem bestimmten Zeitintervall wird nur auf eine Teilmenge von Seiten
        zugegriffen (Working Set)
      * Wenn sich das Working Set nur gering ändert, werden nicht zum Working Set
        gehörende Seiten verdrängt
    * FIFO-Anomalie
      * Wenn der Speicher größer ist und FIFO genutzt wird, kann es in manchen 
        Fällen sogar zu mehr Seitenfehlern kommen als mit einem kleineren Speicher
  * Non-Demand-Paging
    * Austausch mehrerer Seiten
    * One Block Look Ahead (OBL)
      * Wie LRU, solange keine Seitenfehler auftreten
      * Seitenfehler bei $p_i$: ist $p_{i+1}$ im Speicher?
        * Nein: An letzte Stelle laden, vorletzte Seite wegschmeißen
        * Ja: Wie bei FIFO
    * Look-Ahead-OBL:
      * Immer $p_{i+1}$ laden, auch ohne Seitenfehler
  * Thrashing:
    * CPU verbraucht mehr Zeit zum Laden von Seiten als zum Ausführen von Instruktionen
    * Bei zu vielen Prozessen im Speicher
  * Lokale / Globale Speichereinteilung
  * Aktive Seite: Wahrscheinlichkeit, dass sie genutzt werden wird, ist hoch
  * Lifetime-Funktion $L(m)$
    * Mittlere Zeit zwischen zwei Seitenfehlern bei Rahmenzahl $m$
    * Knie-Kriterium
      * Lege Tangente an $L(m)$
      * Tangente liegt am Punkt der optimalen Rahmenzahl $m_{opt}$
    * Working-Set-Konzept
      * Falls die Summe der Working Sets größer ist als die Zahl der Rahmen im System,
        lege einen Prozess still, ansonsten besteht Gefahr des Thrashings
    * Abschätzung von L(m)
      1. Anzahl aktiver Seiten durch Working Set bestimmen
      2. Mittlere Zeit zwischen Seitenfehlern berechnen
      3. Anzahl der Rahmen gegen mittlere Zeit zwischen 2 Seitenf. eintragen,
          Tangente anlegen
  * Page Fault Frequency PFF: Zuteilung von Rahmen zu Prozessen nach
    Seitenfehlerhäufigkeit der Prozesse
    ```
    if PFF > upperLimit {
      if seitenFrei {
        Prozess erhält mehr Seiten
      } else {
        Suspendiere Prozess
      }
    }
    if PFF < lowerLimit {
      gib Seiten frei
    }
    ```
  * Linux Kernel implementiert Demand-Paging
    * Page Daemon (`kswapd`)
    * Page fault: `handle_mm_fault()`
    * Page Frame Reclaming Algorithm (PFRA) versucht, immer einige Seiten frei zu halten
    * Unterschiedliche Seitentypen:
      * Unreclaimable Pages
      * Swappable Pages müssen in die Swap Area geschrieben werden
      * Syncable Pages müssen zurückgeschrieben, falls D-Bit gesetzt ist
      * Discardable Pages können freigegeben werden
    * PFRA Seitenzustände
      * Bei Seitenzugriff:
        * Ist in Inactive und A-Bit gleich 0: Setze in Active mit A-Bit gleich 0
        * Sonst: Setze A-Bit auf 1
      * Bei Scan:
        * A-Bit gesetzt: Setze zurück
        * Sonst, Active: Schiebe in Inactive
        * Sonst, Inactive: Gebe frei

## Dateisystem
* Header

| Flags | ... | Entry Point | Symbol table size | BSS Size | Data size | Text size | Magic number |
| --- | --- | --- | --- | --- | --- | --- | --- |
|       |     |             |                   |          |           |           |              |

* Dateiattribute
  * Name
  * Identifikator
  * Dateityp
  * Attribute
  * Alles im FCB (File Control Block)
* Kodierung von Zugriffsrechten auf POSIX
  * 9 Bit 
  * User (UID) - Group (GID) - All
  * read - write - exec
* File Descriptor
  * `fd = open(filename, flags);`
  * fd integer, kleinster nicht zugewiesener Wert
  * Systemweit gültig
  * Zeigt auf Tabelle offener Dateien (File Descriptor Table)
* inode - Index Node (128 byte)
  * Repräsentiert Dateien, Ordner, Geräte, …
  * Inhalte:
    * Typ, Zugriffrechte, Benutzer- und Gruppen-ID
    * Größe der Datei
    * Datum der letzen Änderung / des letzten Zugriffs
    * Adressen der Datenblöcke (je 4 byte)
    * Blockgröße 1-8 KiB, üblicherweise 4 KiB
    * `stat(name, &buffer)` oder `fstat(fd, &buffer)` zum Auslesen
      der inode-Infos einer Datei
* Speicherung
  * Block üblicherweies 4KiB
  * Verteilung einer Datei über mehrere Blöcke
  * Günstige Strategien zur Platzierung
    * Keine unnötige Rotation
    * Keine unnötigen Bewegungen des Disk Heads
  * Ext2
    * Aufteilung der Festplatte in Blockgruppen
    * Feste Strukturierung in einer Gruppe
    * | Superblock | Group Descr. | nlock bitmap | inode bitmap | inodes | data blocks | 
  * Ext3
    * Operationen und Operationsreihenfolge ins Journal schreiben
    * Bei Abarbeitung Journal leer
    * Bei Absturz:
      * vorherige Aktionen rückgängig machen oder Aktionen jetzt ausführen
  * Ext4
    * ist neu

## I/O
* I/O Hardware
  * Controller
    * Steuerung eines Gerätetyps 
    * Auf Motherboard oder PCI-Karte
    * Anschluss für ein oder mehrere Geräte
    * Schnittstelle zwischen Rechner und Gerät
  * I/O Werk
    * Komponente zwischen CPU und Controller
    * Weitgehend selbständige Steuerung von I/O Operationen 
      unabhängig von der CPU
    * Delegation der Ausführung eines I/O Befehls von der CPU
      an das I/O Werk
  * Register 
    * Zur Kommunikation mit der CPU
    * DataIn, DataOut
    * Status
    * Control
  * Buffer 
    * zur Speicherung von Daten bis zur Verarbeitung
  * Autonomer Datentransfer
    * CPU stößt I/O-Operation an, I/O-Werk führt sie aus
    * DMA = Direct Memory Access
      * CPU erzeugt Kommandoblock für Gerät mit Quelle, Ziel, Anzahl der Bytes
      * CPU delegiert Block an DMA-Controller und fährt mit anderen Aufgaben fort
      * DMA überwacht Festplattencontroller und 
        sendet Interrupt an die CPU, wenn die Übertragung beendet ist
    * Oder: Spezieller Koprozessor
  * Adressierung
    * Jedes Register bekommt I/O - Portnummer
    * Oder: Memory mapped I/O
      * Vorteile:
        * Einfachere Implementierung
        * Einfache Verweigerung von Zugriff auf Controller-Register
      * Nachteile:
        * I/O Gerät sieht Änderungen im Speicher nicht direkt
        * Caching von Teilen des Hauptspeichers kann zu Fehlern führen
* Treiber
  * Eigener Treiber für jede Geräteart
    * Verbergen von Unterschiede
  * Einheitliche Schnittstelle zu allen Treibern
  * Abbildung Symbolischer Namen auf Geräte
* Adressumsetzung
  * Dateisystem: Blöcke a 4 KiB
  * Festplatte: Sektoren a 512 Byte
  * Logical Block Adressing (LBA)
    * Sektoren der Festplatte werden einfach durchnummeriert
    * Lineare Abbildung von Blöcken auf Sequenzen von Sektoren
* Festplatte
  * Zeit zum Lesen / Schreiben eines Blocks
    * Seek Time $T_p$: Positionierung des Disk Heads auf Zylinder
    * Rotational Latency $T_r$: Wartezeit bis gewünschter Sektor unter Disk Head erscheint
    * Transferzeit
  * First come first serve (FCFS)
    * Arbeite Operationen der Reihe nach ab
  * Shortest Seek Time First (SSTF)
  * SCAN
    * Unterwegs in eine Richtung, am Ende Richtung ändern
  * LOOK
    * Unterwegs in eine Richtung, ab letztem Zugriff Richtung ändern
  * C-SCAN
    * Wie Scan, nur ohne Richtungswechsel (modulo)
  * Noop-Scheduling (*)
  * Deadline-Scheduling (*)

## Kommunikation

| Schicht                 | Funktion                                                             |
| ----------------------- | -------------------------------------------------------------------- |
| Anwendungsschicht       | Anwendungsspezifische Funktionen zsmgefasst in Anwendungsprotokollen |
| Transportschicht        | Ende-zu-Ende Dateiübetragung zwischen zwei Rechnern                  |
| Netzwerkschicht         | Wegewahl im Netzwerk                                                 |
| Host-to-Network-Schicht | Schnittstelle zum physikalischen Medium                              |

* Adressprotokolle
  * IPv4, IPv6
* Kommnikationsprotokolle
  * TCP: "Transmission Control Protocol"
    * bidirektionaler, zuverlässiger Kommunikationskanal
    * Datenverluste werden erkannt und automatisch behoben
  * UDP "User Datagram Protocol"
    * unidirektional, unzuverlässig
    * Schneller Datenaustausch ohne großen Overhead
* Linux
  * `socket(...)` Erstellung eines Sockets
  * `bind(...)` Verknüpfe Socket mit Adressinformation
  * `listen(...)` warte auf eingehende Verbindungsanfragen
  * `accept(...)` Annahme einer Verbindungsanfrage
  * `connect(...)` Öffnen einer Verbindung (Client)
  * `close(...)` 
  * `shutdown(...)`
